import geopandas as gpd
import pandas as pd
from sklearn.preprocessing import LabelEncoder
import numpy as np

# import the KNeighborsClassifier class from sklearn.neighbors
from sklearn.neighbors import KNeighborsClassifier

# import the accuracy_score function from sklearn.metrics
from sklearn.metrics import accuracy_score

# create a dictionary to map change_type values to integers
change_type_map = {'Demolition': 0, 'Road': 1, 'Residential': 2, 'Commercial': 3, 'Industrial': 4,
       'Mega Projects': 5}

## Read csvs

# read the train.geojson file using geopandas and store it in train_df
# set the index_col to 0 to use the Id column as the index
train_df = gpd.read_file('train.geojson', index_col=0)
# read the test.geojson file using geopandas and store it in test_df
# set the index_col to 0 to use the Id column as the index
test_df = gpd.read_file('test.geojson', index_col=0)

## Filtering column "mail_type"

# convert the 'geometry' column of the train_df dataframe to a numpy array and store it in train_x
# take the area of the geometries
train_x = np.asarray(train_df[['geometry']].area)
# reshape train_x to be a 2D array with 1 column
train_x = train_x.reshape(-1, 1)
# apply a lambda function to the 'change_type' column of the train_df dataframe 
# to map the change_type values to integers using the change_type_map dictionary
# and store the result in train_y
train_y = train_df['change_type'].apply(lambda x: change_type_map[x])

# convert the 'geometry' column of the test_df dataframe to a numpy array and store it in test_x
# take the area of the geometries
test_x = np.asarray(test_df[['geometry']].area)
# reshape test_x to be a 2D array with 1 column
test_x = test_x.reshape(-1, 1)

# print the shape of train_x, train_y and test_x
print (train_x.shape, train_y.shape, test_x.shape)


## Train a simple OnveVsRestClassifier using featurized data

# create an instance of the KNeighborsClassifier class with n_neighbors=3
neigh = KNeighborsClassifier(n_neighbors=3)
# fit the model to the training data (train_x and train_y)
neigh.fit(train_x, train_y)
# predict the change_type for the test_x data and store the result in pred_y
pred_y = neigh.predict(test_x)
# print the shape of pred_y
print (pred_y.shape)

## Save results to submission file

# create a dataframe from pred_y with one column 'change_type'
pred_df = pd.DataFrame(pred_y, columns=['change_type'])
# write the pred_df dataframe to a csv file named 'knn_sample_submission.csv'
# set the index to be the Id column and label the index column as 'Id'
pred_df.to_csv("knn_sample_submission.csv", index=True, index_label='Id')
